package dk.dtu.group19.chefmeapp;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class LoginActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Typeface lato = Typeface.createFromAsset(getAssets(),"fonts/Lato-Regular.ttf");

        TextView orText= findViewById(R.id.orTextView);
        TextView myTextView2= findViewById(R.id.textView2);
        ImageButton facebookLoginButton = findViewById(R.id.facebookLoginButton);
        Button loginButton = findViewById(R.id.loginButton);


        facebookLoginButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        System.out.println("Der blev trykket på en knap");
    }
}
